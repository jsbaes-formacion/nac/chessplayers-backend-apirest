package eu.malaga.chessplayers.backend.apirest.controladores;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import eu.malaga.chessplayers.backend.apirest.entidades.Escuela;
import eu.malaga.chessplayers.backend.apirest.servicios.IEscuelaService;

//Mediante la notación CrossOrigin indicamos las URL que pueden acceder a nuestra API
@CrossOrigin(origins = { "http://localhost:4200" }) // URLs que puede acceder a la api
@RestController // Se trata de un controlador REST
@RequestMapping("/api") // Prefijo de las URLs con las que se accedera a la API
public class EscuelaRestController {
	// Tamaño de la página para el méotodo que devuelve los datos paginados
	final Integer PAGE_SIZE = 5;

	// Inyectamos la implementación de la interfaz del servicio de Jugadores
	@Autowired
	private IEscuelaService escuelaService;

	/**
	 * Respuesta al verbo GET de la API para obtener la lista de escuelas de la BD
	 * 
	 * @return Devuelve la lista de escuelas o los mensajes de error junto con el
	 *         status
	 */
	@GetMapping("/escuelas")
	public ResponseEntity<?> index() {
		// Prepara la estructura de salida
		Map<String, Object> response = new HashMap<>();
		// Iniciliza la lista de jugadores
		List<Escuela> escuelas = null;
		try {
			// Obtención de los jugadores
			escuelas = escuelaService.recuperaTodos();
		} catch (DataAccessException e) {
			// Si se ha producido un error, se devuelve un mensaje de error junto con
			// la descripción detallada del mismo
			response.put("mensaje", "Error al realizar la consulta en la Base de Datos");
			response.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		// Si no se ha producido error al obtener los datos, se devuelve la lista y el
		// satus 200 (OK)
		return new ResponseEntity<List<Escuela>>(escuelas, HttpStatus.OK);
	}

	/**
	 * Respuesta al verbo GET de la API para obtener una escuela a partir de su id
	 * 
	 * @param id Identificador de la escuela a obtener
	 * @return La escuela obtenida o un mensaje de error junto con su status
	 */
	@GetMapping("/escuelas/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {
		// Prepara la estructura de salida
		Map<String, Object> response = new HashMap<>();
		// Iniciliza la escuela
		Escuela escuela = null;
		try {
			// Obtención de la escuela mediante llamada al servicio
			escuela = escuelaService.buscarPorId(id);
		} catch (DataAccessException e) {
			// Si se ha producido un error, se devuelve un mensaje de error junto con
			// la descripción detallada del mismo
			response.put("mensaje", "Error al realizar la consulta en la Base de Datos");
			response.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		// En caso de no haberse producido erroes, se comprueba si se ha encontrado la
		// escuela
		if (escuela == null) {
			// Si no se ha encontrado, se devuelve un mensaje de error.
			response.put("mensaje",
					"La escuela ID: ".concat(id.toString().concat(" no se encuentra en la Base de Datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		// Si no se han producido errores y se ha encontrado la escuela, se devuelve
		// ésta junto con
		// un código de status 200 (OK)
		return new ResponseEntity<Escuela>(escuela, HttpStatus.OK);
	}

	/**
	 * Respuesta al verbo GET de la API para obtener una lista paginada de escuelas
	 * 
	 * @param page Número de página a obtener
	 * @return El escuela obtenido o un mensaje de error junto con su status
	 */
	@GetMapping("/escuelas/page/{page}")
	public Page<Escuela> index(@PathVariable Integer page) {
		// Se obtiene la página con el tamaño establecido
		Pageable pageable = PageRequest.of(page, PAGE_SIZE);
		// Se devuelve la página con la lista de escuelas correspondiente
		return escuelaService.recuperaTodos(pageable);
	}

	/**
	 * Respuesta al verbo POST de la API para dar de alta una escuela
	 * 
	 * @param escuela Datos de la escuela a actualizar
	 * @return La escuela obtenida o un mensaje de error junto con su status
	 */
	@PostMapping("/escuelas")
	public ResponseEntity<?> create(@RequestBody Escuela escuela) {
		// Incializa la escuela a devolver
		Escuela escuelaNew = null;

		// Inicializa la estructura de salida
		Map<String, Object> response = new HashMap<>();

		try {
			// Actualización de la escuela mediante llamada al servicio
			escuelaNew = escuelaService.guardar(escuela);
		} catch (DataAccessException e) {
			// Si se ha producido un error en el alta, se devuelve un mensaje de error
			response.put("mensaje", "Error al realizar el insert en la Base de Datos");
			response.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		// Si no se han producido errores se devuelve la propia escuela actualizada
		// junto con un mensaje de confirmación y el código de status 201 (CREATED)
		response.put("mensaje", "La escuela ha sido creada con éxito");
		response.put("escuela", escuelaNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	/**
	 * Respuesta al verbo PUT de la API para actualizar una escuela
	 * 
	 * @param escuela Datos de la escuela a actualizar en el cuerpo de la llamada
	 * @param id      Campo id de la escuela a actualizar
	 * @return La escuela actualizada o un mensaje de error junto con su status
	 */
	@PutMapping("/escuelas/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> update(@RequestBody Escuela escuela, @PathVariable Long id) {
		// Incializa la escuela a devolver
		Escuela escuelaActual = null;

		// Inicializa la estructura de salida
		Map<String, Object> response = new HashMap<>();

		try {
			// Llamada al servicio para buscar la escuela por su id
			escuelaActual = escuelaService.buscarPorId(id);
		} catch (DataAccessException e) {
			// Si se ha producido un error al buscar la escuela, se devuelve un mensaje de
			// error
			response.put("mensaje", "Error al actualizar la Base de Datos");
			response.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		// En caso de no haberse producido erroes, se comprueba si se ha encontrado la
		// escuela
		if (escuelaActual == null) {
			// Si no se ha encontrado, se devuelve un mensaje de error.
			response.put("mensaje", "Error: No se pudo editar, la escuela ID: "
					.concat(id.toString().concat(" no se encuentra en la Base de Datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		// Si no se han producido errores y se ha encontrado la escuela, se actualizan
		// sus propiedades
		// con los nuevos valores corresondientes a la misma y se actualiza mediante el
		// servicio
		try {
			;
			escuelaActual.setDescripcion(escuela.getDescripcion());
			escuelaService.guardar(escuelaActual);
		} catch (DataAccessException e) {
			// Si se ha producido un error al actualizar escuela, se devuelve un mensaje de
			// error
			response.put("mensaje", "Error al actualizar la Base de Datos");
			response.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		// Si no se han producido errores, se devuelve la propia escuela actualizada
		// junto con un mensaje de confirmación y el código de status 201 (CREATED)
		response.put("mensaje", "La escuela ha sido actualizada con éxito");
		response.put("escuela", escuelaActual);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	/**
	 * Respuesta al verbo DELETE de la API para borrar un escuela
	 * 
	 * @param id Campo id de la escuela a eliminar
	 * @return Mensaje de confirmación del borrado o un mensaje de error junto con
	 *         su status
	 */
	@DeleteMapping("/escuelas/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {

		// Inicializa la estructura de salida
		Map<String, Object> response = new HashMap<>();

		try {
			// Borrado de la escuela mediante llamada al servicio
			escuelaService.borrar(id);
		} catch (DataAccessException e) {
			// Si se ha producido un error al borrar la escuela, se devuelve un mensaje de
			// error
			response.put("mensaje", "Error al actualizar la Base de Datos");
			response.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		// Si no se han producido errores, se devuelve un mensaje de confirmación
		// de que ha ido bien y el código de status 200 (OK)
		response.put("mensaje", "La escuela ha sido eliminada con éxito");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	/**
	 * Devuelve un PDF con datos de las escuelas
	 */
	@GetMapping("/escuelas/report")
	public byte[] escuelasReport() {
		return escuelaService.generateReport();
	}
}
