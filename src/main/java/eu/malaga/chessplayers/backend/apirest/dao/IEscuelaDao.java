package eu.malaga.chessplayers.backend.apirest.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.malaga.chessplayers.backend.apirest.entidades.Escuela;

/**
 * Interfaz DAO que exitiende los métodos del JpaRepository de Spring Data
 * @author jsbarriga
 * @version 1.0
 * @since   2019-06-13 
 */
public interface IEscuelaDao extends JpaRepository<Escuela, Long> {

}