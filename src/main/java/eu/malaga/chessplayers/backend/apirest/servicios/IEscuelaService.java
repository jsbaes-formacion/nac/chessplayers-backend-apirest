package eu.malaga.chessplayers.backend.apirest.servicios;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import eu.malaga.chessplayers.backend.apirest.entidades.Escuela;

/**
 * Interfaz que define los método ques va a implementar el servicio de escuelas
 * para comuicarse con la BD a través de los métodos del DAO
 * @author jsbarriga
 * @version 1.0
 * @since   2019-06-13 
 */
public interface IEscuelaService {
	
	public List<Escuela> recuperaTodos();
	
	public Page<Escuela> recuperaTodos(Pageable pageable);
	
	public Escuela buscarPorId(Long id);
	
	public Escuela guardar(Escuela escuela);
	
	public void borrar(Long id);
	
	public byte[] generateReport();
	
}
