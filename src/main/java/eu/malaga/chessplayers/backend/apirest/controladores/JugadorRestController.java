package eu.malaga.chessplayers.backend.apirest.controladores;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import eu.malaga.chessplayers.backend.apirest.entidades.Jugador;
import eu.malaga.chessplayers.backend.apirest.servicios.IJugadorService;

/**
 * Clase que implementa los métodos del controlador
 * 
 * @author jsbarriga
 * @version 1.0
 * @since 2019-05-13
 */

// Mediante la notación CrossOrigin indicamos las URL que pueden acceder a nuestra API
@CrossOrigin(origins = { "http://localhost:4200" }) // URLs que puede acceder a la api
@RestController // Se trata de un controlador REST
@RequestMapping("/api") // Prefijo de las URLs con las que se accedera a la API
public class JugadorRestController {

	// Tamaño de la página para el méotodo que devuelve los datos paginados
	final Integer PAGE_SIZE = 5;

	// Inyectamos la implementación de la interfaz del servicio de Jugadores
	@Autowired
	private IJugadorService jugadorService;

	/**
	 * Respuesta al verbo GET de la API para obtener la lista de jugadores de la BD
	 * 
	 * @return Devuelve la lista de jugadores o los mensajes de error junto con el
	 *         status
	 */
	@GetMapping("/players")
	public ResponseEntity<?> index() {
		// Prepara la estructura de salida
		Map<String, Object> response = new HashMap<>();
		// Iniciliza la lista de jugadores
		List<Jugador> jugadores = null;
		try {
			// Obtención de los jugadores
			jugadores = jugadorService.recuperaTodos();
		} catch (DataAccessException e) {
			// Si se ha producido un error, se devuelve un mensaje de error junto con
			// la descripción detallada del mismo
			response.put("mensaje", "Error al realizar la consulta en la Base de Datos");
			response.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		// Si no se ha producido error al obtener los datos, se devuelve la lista y el
		// satus 200 (OK)
		return new ResponseEntity<List<Jugador>>(jugadores, HttpStatus.OK);
	}

	/**
	 * Respuesta al verbo GET de la API para obtener un jugador a partir de su id
	 * 
	 * @param id Identificador del jugador a obtener
	 * @return El jugador obtenido o un mensaje de error junto con su status
	 */
	@GetMapping("/players/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {
		// Prepara la estructura de salida
		Map<String, Object> response = new HashMap<>();
		// Iniciliza el jugador
		Jugador jugador = null;
		try {
			// Obtención del jugador mediante llamada al servicio
			jugador = jugadorService.buscarPorId(id);
		} catch (DataAccessException e) {
			// Si se ha producido un error, se devuelve un mensaje de error junto con
			// la descripción detallada del mismo
			response.put("mensaje", "Error al realizar la consulta en la Base de Datos");
			response.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		// En caso de no haberse producido erroes, se comprueba si se ha encontrado el
		// jugador
		if (jugador == null) {
			// Si no se ha encontrado, se devuelve un mensaje de error.
			response.put("mensaje",
					"El jugador ID: ".concat(id.toString().concat(" no se encuentra en la Base de Datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		// Si no se han producido errores y se ha encontrado el jugador, se devuelve
		// éste junto con
		// un código de status 200 (OK)
		return new ResponseEntity<Jugador>(jugador, HttpStatus.OK);
	}

	/**
	 * Respuesta al verbo GET de la API para obtener una lista paginada de jugadores
	 * 
	 * @param page Número de página a obtener
	 * @return El jugador obtenido o un mensaje de error junto con su status
	 */
	@GetMapping("/players/page/{page}")
	public Page<Jugador> index(@PathVariable Integer page) {
		// Se obtiene la página ocn el tamaño establecido
		Pageable pageable = PageRequest.of(page, PAGE_SIZE);
		// Se devuelve la página con la lista de jugadores correspondiente
		return jugadorService.recuperaTodos(pageable);
	}

	/**
	 * Respuesta al verbo POST de la API para dar de alta un jugador
	 * 
	 * @param jugador Datos del jugador a actualizar
	 * @return El jugador obtenido o un mensaje de error junto con su status
	 */
	@PostMapping("/players")
	public ResponseEntity<?> create(@RequestBody Jugador jugador) {
		// Incializa el jugador a devolver
		Jugador jugadorNew = null;

		// Inicializa la estructura de salida
		Map<String, Object> response = new HashMap<>();

		try {
			// Actualización del jugador mediante llamada al servicio
			jugadorNew = jugadorService.guardar(jugador);
		} catch (DataAccessException e) {
			// Si se ha producido un error en el alta, se devuelve un mensaje de error
			response.put("mensaje", "Error al realizar el insert en la Base de Datos");
			response.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		// Si no se han producido errores se devuelve el propio jugador actualizado
		// junto con un mensaje de confirmación y el código de status 201 (CREATED)
		response.put("mensaje", "El jugador ha sido creado con éxito");
		response.put("jugador", jugadorNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	/**
	 * Respuesta al verbo PUT de la API para actualizar un jugador
	 * 
	 * @param jugador Datos del jugador a actualizar en el cuerpo de la llamada
	 * @param id      Campo id del jugador a actualizar
	 * @return El jugador actualizado o un mensaje de error junto con su status
	 */
	@PutMapping("/players/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> update(@RequestBody Jugador jugador, @PathVariable Long id) {
		// Incializa el jugador a devolver
		Jugador jugadorActual = null;

		// Inicializa la estructura de salida
		Map<String, Object> response = new HashMap<>();

		try {
			// Llamada al servicio para buscar el jugador por su id
			jugadorActual = jugadorService.buscarPorId(id);
		} catch (DataAccessException e) {
			// Si se ha producido un error al buscar el jugador, se devuelve un mensaje de
			// error
			response.put("mensaje", "Error al actualizar la Base de Datos");
			response.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		// En caso de no haberse producido erroes, se comprueba si se ha encontrado el
		// jugador
		if (jugadorActual == null) {
			// Si no se ha encontrado, se devuelve un mensaje de error.
			response.put("mensaje", "Error: No se pudo editar, el jugador ID: "
					.concat(id.toString().concat(" no se encuentra en la Base de Datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		// Si no se han producido errores y se ha encontrado el jugador, se actualizan
		// sus propiedades
		// con los nuevos valores corresondientes a las mismas y se actualiza mediante
		// el servicio
		try {
			jugadorActual.setNombre(jugador.getNombre());
			jugadorActual.setDescripcion(jugador.getDescripcion());
			jugadorActual.setImg(jugador.getImg());
			jugadorActual.setNacimiento(jugador.getNacimiento());
			jugadorActual.setEscuela(jugador.getEscuela());
			jugadorActual.setElo(jugador.getElo());

			jugadorService.guardar(jugadorActual);
		} catch (DataAccessException e) {
			// Si se ha producido un error al actualizar jugador, se devuelve un mensaje de
			// error
			response.put("mensaje", "Error al actualizar la Base de Datos");
			response.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		// Si no se han producido errores, se devuelve el propio jugador actualizado
		// junto con un mensaje de confirmación y el código de status 201 (CREATED)
		response.put("mensaje", "El jugador ha sido actualizado con éxito");
		response.put("jugador", jugadorActual);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

	/**
	 * Respuesta al verbo DELETE de la API para borrar un jugador
	 * 
	 * @param id Campo id del jugador a eliminar
	 * @return Mensaje de confirmación del borrado o un mensaje de error junto con
	 *         su status
	 */
	@DeleteMapping("/players/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {

		// Inicializa la estructura de salida
		Map<String, Object> response = new HashMap<>();

		try {
			// Borrado del jugador mediante llamada al servicio
			jugadorService.borrar(id);
		} catch (DataAccessException e) {
			// Si se ha producido un error al borrar el jugador, se devuelve un mensaje de
			// error
			response.put("mensaje", "Error al actualizar la Base de Datos");
			response.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		// Si no se han producido errores, se devuelve un mensaje de confirmación
		// de que ha ido bien y el código de status 200 (OK)
		response.put("mensaje", "El jugador ha sido eliminado con éxito");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	/**
	 * Devuelve un PDF con datos de los jugadores
	 * 
	 * @return Report generado como array de bytes.
	 */
	@GetMapping("/players/report")
	public byte[] jugadoresReport() {
		return jugadorService.generateReport();
	}
}
