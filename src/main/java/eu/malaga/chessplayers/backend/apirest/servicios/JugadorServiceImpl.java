package eu.malaga.chessplayers.backend.apirest.servicios;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import eu.malaga.chessplayers.backend.apirest.dao.IJugadorDao;
import eu.malaga.chessplayers.backend.apirest.entidades.Jugador;
import eu.malaga.chessplayers.backend.apirest.reports.RepJugadorListado;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * Clase que implementa el servicio que se comunica con al BD implementando la
 * interfaz IJugadorService
 * 
 * @author jsbarriga
 * @version 1.0
 * @since 2019-05-13
 */
@Service
public class JugadorServiceImpl implements IJugadorService {

	// Inyectamos la implementación de la interfaz del DAO de Jugadores
	@Autowired
	private IJugadorDao jugadorDao;

	/**
	 * Devuelve la lista de jugadores de la BD
	 * 
	 * @return Devuelve la lista de jugadores
	 */
	@Override
	public List<Jugador> recuperaTodos() {
		// Constante con el valor para escuela vacía
		final String ELO_VACIO = "DESCONOCIDO";

		// Se obtiene de la BD la lista de jugadores llamando al método
		// "findAll" del DAO y luego se recorre detectado aquellos jugadores
		// cuyo campo "elo" esta vacío, actualiandolo en ese
		// caso con el valor constante especificado en ELO_VACIO
		List<Jugador> jugadores = (List<Jugador>) jugadorDao.findAll();
		for (Jugador jugador : jugadores) {
			if (jugador.getElo().isEmpty()) {
				jugador.setElo(ELO_VACIO);
			}
		}
		// Devuelve la lista de jugadores
		return jugadores;
	}

	/**
	 * Obtiene la lsita de jugadores de la BD de forma paginada
	 * 
	 * @param pageable Página que se quiere obtener
	 * @return Lista de jugadores correspondiente a la página
	 */
	@Override
	public Page<Jugador> recuperaTodos(Pageable pageable) {
		// Constante con el valor para escuela vacía
		final String ELO_VACIO = "DESCONOCIDO";

		// Se obtiene de la BD la lista de jugadores correspondientes a
		// la página especifcada llmando al método "findAll" del DAO
		// y luego se recorre detectado aquellos jugadores
		// cuyo campo "elo" esté vacío, actualiandolo en ese
		// caso con el valor constante especificado en ELO_VACIO
		Page<Jugador> jugadores = (Page<Jugador>) jugadorDao.findAll(pageable);
		for (Jugador jugador : jugadores) {
			if (jugador.getElo().isEmpty()) {
				jugador.setElo(ELO_VACIO);
			}
		}

		// Devuelve la lista de jugadores correspondientes a la página seleccionada
		return jugadores;
	}

	/**
	 * Devuelve el jugador de la BD cuyo id coincide con el parámetro
	 * 
	 * @param id Campo id del jugador a seleccionar
	 * @return Jugador cuyo id corresponde con el especificado
	 */
	@Override
	public Jugador buscarPorId(Long id) {
		return jugadorDao.findById(id).orElse(null);
	}

	/**
	 * Guarda en la BD el jugador que se pasa como parámetro ya sea un nuevo jugador
	 * o uno existente modificado
	 * 
	 * @param jugador Datos del jugador a dar de alta o modificar
	 * @return Nuevo jugador o el existente modificado
	 */
	@Override
	public Jugador guardar(Jugador jugador) {
		return jugadorDao.save(jugador);
	}

	/**
	 * Borra de la BD el jugador cuyo id se pasa como parámetro
	 * 
	 * @param id Campo id del jugador a eliminar
	 */
	@Override
	public void borrar(Long id) {
		jugadorDao.deleteById(id);
	}

	/**
	 * Genera un pdf con los datos de las escuelas
	 * 
	 * @return El listado en formato pdf y en un stream de salida como array de
	 *         bytes
	 */
	public byte[] generateReport() {

		// Ruta de resources donde se tienen los informes (.jasper) o las plantillas de
		// los informes (.jrxml)
		String reportPath = "/jasper";

		// Salida del método con el pdf ya generado
		ByteArrayOutputStream pdf = new ByteArrayOutputStream();

		// Se obtienen los datos para el informe
		// List<Jugador> jugadorList = jugadorDao.findAll();
		List<RepJugadorListado> datosRepJugadores = new ArrayList<>();

		List<Jugador> jugadores = (List<Jugador>) jugadorDao.findAll();
		for (Jugador jugador : jugadores) {

			datosRepJugadores.add(new RepJugadorListado(jugador.getNombre(), jugador.getDescripcion(),
					new SimpleDateFormat("dd-MM-yyyy").format(jugador.getNacimiento()),
					jugador.getEscuela().getDescripcion(), jugador.getElo()));

		}

		// Opcion A: El informe se compila previamente con Jaspersoft Studio y se carga
		// el .jasper:
		final InputStream jasperReport = getClass().getResourceAsStream(reportPath + "/RepJugadores.jasper");

		// Opción B: Se carga la plantilla del informe (.jrxml) y se compila "al vuelo":
		// final InputStream reportInputStream =
		// getClass().getResourceAsStream(reportPath + "/RepJugadores.jrxml");
		// final JasperDesign jasperDesign = JRXmlLoader.load(reportInputStream);
		// JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);

		// Se preapara el datasource del informe con los datos de los jugadores
		// final JRBeanCollectionDataSource jrBeanCollectionDataSource = new
		// JRBeanCollectionDataSource(jugadorList);
		final JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(datosRepJugadores);

		// Si el informe tuviera parámetros, se introducen mediante esta estructura:
		Map<String, Object> parameters = new HashMap<>();
		// En el informe se ha definido un parámetro "autor":
		parameters.put("autor", "CEMI");

		try {
			// Genera el PDF a partir de los elementos anteriores
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
					jrBeanCollectionDataSource);

			// Si quisiérmos exportar el PDF a un fichero en la raíz del proyecto:
			// JasperExportManager.exportReportToPdfFile(jasperPrint,
			// "InformeJugadores.pdf");

			// Convierte el pdf generado en un stream
			JasperExportManager.exportReportToPdfStream(jasperPrint, pdf);

		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getMessage());
		}

		// Devuelve el PDF como un stream
		return pdf.toByteArray();
	}
}
