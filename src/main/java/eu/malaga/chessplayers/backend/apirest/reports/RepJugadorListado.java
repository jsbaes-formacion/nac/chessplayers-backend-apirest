package eu.malaga.chessplayers.backend.apirest.reports;

/**
 * Clase que implementa los datos del informe RepJugadores
 * @author jsbarriga
 * @version 1.0
 * @since   2019-10-04 
 */
public class RepJugadorListado {
	private String nombre;
	private String descripcion;
	private String nacimiento;
	private String escuela;
	private String elo;
	
	public RepJugadorListado(String nombre, String descripcion, String nacimiento, String escuela, String elo) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.nacimiento = nacimiento;
		this.escuela = escuela;
		this.elo = elo;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getNacimiento() {
		return nacimiento;
	}
	public void setNacimiento(String nacimiento) {
		this.nacimiento = nacimiento;
	}
	public String getEscuela() {
		return escuela;
	}
	public void setEscuela(String escuela) {
		this.escuela = escuela;
	}
	public String getElo() {
		return elo;
	}
	public void setElo(String elo) {
		this.elo = elo;
	}
}
