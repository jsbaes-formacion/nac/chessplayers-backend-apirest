package eu.malaga.chessplayers.backend.apirest.servicios;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import eu.malaga.chessplayers.backend.apirest.entidades.Jugador;

/**
 * Interfaz que define los método ques va a implementar el servicio de jugadores
 * para comuicarse con la BD a través de los métodos del DAO
 * @author jsbarriga
 * @version 1.0
 * @since   2019-05-13 
 */
public interface IJugadorService {
	
	public List<Jugador> recuperaTodos();
	
	public Page<Jugador> recuperaTodos(Pageable pageable);
	
	public Jugador buscarPorId(Long id);
	
	public Jugador guardar(Jugador jugador);
	
	public void borrar(Long id);
	
	public byte[] generateReport();
	
}

