package eu.malaga.chessplayers.backend.apirest.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Clase que implementa la clase entidad Jugador
 * @author jsbarriga
 * @version 1.0
 * @since   2019-05-13 
 */
@Entity
@Table(name="jugadores")  // Se especifica el nombre de la tabla
public class Jugador implements Serializable {
	// Constante por defecto para la serialización de la clase
	private static final long serialVersionUID = 1L;

	// Campo correspondiente a la clave primaria de la entidad
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	// La columan nombre no podrá estar vacía
	@Column(nullable = false)
	private String nombre;
	
	// La columna descripcion tendrá un máximo de 512 caracteres
	@Column(length = 512)
	private String descripcion;
	private String img;
	
	// La columna nacimiento no podrá estar vacía
	@Column(nullable = false)
	private Date nacimiento;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "escuela_id") // Si no lo ponemos, va a crear uno con éste mismo nombre
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler" }) // Se ponen por haber puesto FetchType.LAZY
	private Escuela escuela;
	
	private String elo;
	
	public Jugador(Long id, String nombre, String descripcion, String img, Date nacimiento, Escuela escuela,
			String elo) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.img = img;
		this.nacimiento = nacimiento;
		this.escuela = escuela;
		this.elo = elo;
	}

	public String getElo() {
		return elo;
	}

	public void setElo(String elo) {
		this.elo = elo;
	}

	// Constructor por defecto 
	public Jugador() {
		
	}
	
	// Constructor de la clase
	public Jugador(Long id, String nombre, String descripcion, String img, Date nacimiento, Escuela escuela) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.img = img;
		this.nacimiento = nacimiento;
		this.escuela = escuela;
	}
	
	
	// Métodogs get y set:
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public Date getNacimiento() {
		return nacimiento;
	}
	public void setNacimiento(Date nacimiento) {
		this.nacimiento = nacimiento;
	}

	public Escuela getEscuela() {
		return escuela;
	}

	public void setEscuela(Escuela escuela) {
		this.escuela = escuela;
	}

}
 