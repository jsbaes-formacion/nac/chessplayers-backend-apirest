package eu.malaga.chessplayers.backend.apirest.entidades;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Clase que implementa la clase entidad Jugador
 * @author jsbarriga
 * @version 1.0
 * @since   2019-06-13 
 */
@Entity
@Table(name="escuelas")  // Se especifica el nombre de la tabla
public class Escuela implements Serializable {
	// Constante por defecto para la serialización de la clase
    private static final long serialVersionUID = 1L;
        
    // Campo correspondiente a la clave primaria de la entidad
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    // La columan nombre no podrá estar vacía
	@Column(nullable = false)
    private String descripcion;
    
    // Muchos jugadores están asociados a una sola escuela => ManyToOne
	// Se carga de forma perezosa para que no recupere datos de escuelas
	// de forma innecesaria:
	@JsonIgnoreProperties(value={"escuela","hibernateLazyInitializer", "handler" }, allowSetters=true) 
	@OneToMany(fetch = FetchType.LAZY, mappedBy="escuela", cascade = CascadeType.ALL)
	private List<Jugador> jugadores;

    public Escuela() {
    }

    public Escuela(Long id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}