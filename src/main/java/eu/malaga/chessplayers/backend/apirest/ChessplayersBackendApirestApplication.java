package eu.malaga.chessplayers.backend.apirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChessplayersBackendApirestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChessplayersBackendApirestApplication.class, args);
	}

}
