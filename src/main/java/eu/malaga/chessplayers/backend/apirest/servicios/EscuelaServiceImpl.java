package eu.malaga.chessplayers.backend.apirest.servicios;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import eu.malaga.chessplayers.backend.apirest.dao.IEscuelaDao;
import eu.malaga.chessplayers.backend.apirest.entidades.Escuela;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * Clase que implementa el servicio que se comunica con al BD 
 * implementando la interfaz IEscuelaService
 * 
 * @author 	jsbarriga
 * @version 1.0
 * @since   2019-06-13 
 */
@Service  
public class EscuelaServiceImpl implements IEscuelaService {

	// Inyectamos la implementación de la interfaz del DAO de Jugadores
	@Autowired 
	private IEscuelaDao escuelaDao;
	
	/**
	 * Devuelve la lista de jugadores de la BD
	 * @return Devuelve la lista de jugadores
	 */
	@Override
	public List<Escuela> recuperaTodos() {
	
		 // Devuelve la lista de escuelas mediante el método "findAll" del DAO
		return (List<Escuela>)escuelaDao.findAll();
	}
	
	/**
	 * Obtiene la lsita de jugadores de la BD de forma paginada
	 * @param pageable Página que se quiere obtener
	 * @return Lista de jugadores correspondiente a la página
	 */
	@Override
	public Page<Escuela> recuperaTodos(Pageable pageable) {
				
		// Se obtiene de la BD la lista de jugadores correspondientes a 
		// la página especifcada llmando al método "findAll" del DAO
	
		// Devuelve la lista de escuelas correspondientes a la página seleccionada
		return (Page<Escuela>)escuelaDao.findAll(pageable);
	}

	/**
	 * Devuelve la escuela de la BD cuyo id coincide con el parámetro
	 * @param id Campo id de la escuela a seleccionar
	 * @return Escuela cuyo id corresponde con el especificado
	 */
	@Override
	public Escuela buscarPorId(Long id) {
		return escuelaDao.findById(id).orElse(null);
	}

	/**
	 * Guarda en la  BD el jugador que se pasa como parámetro
	 * ya sea un nuevo jugador o uno existente modificado
	 * @param jugador Datos del jugador a dar de alta o modificar
	 * @return Nuevo jugador o el existente modificado
	 */
	@Override
	public Escuela guardar(Escuela escuela) {
		return escuelaDao.save(escuela);
	}

	/**
	 * Borra de la BD la escuela cuyo id se pasa como parámetro
	 * @param id Campo id del jugador a eliminar
	 */
	@Override
	public void borrar(Long id) {
		escuelaDao.deleteById(id);
	}
	
	/**
	 * Genera un pdf con los datos de las escuelas
	 * 
	 * @return El listado en formato pdf y en un stream de salida como array de bytes
	 */
	public byte[] generateReport() {

		// Ruta de resources donde se tienen los informes (.jasper) o las plantillas de
		// los informes (.jrxml)
		String reportPath = "/jasper";

		// Salida del método con el pdf ya generado
		ByteArrayOutputStream pdf = new ByteArrayOutputStream();

		// Se obtienen los datos para el informe
		List<Escuela> escuelaList = escuelaDao.findAll();
		
		
		// Opcion A: El informe se compila previamente con Jaspersoft Studio y se carga
		// el .jasper:
		final InputStream jasperReport = getClass().getResourceAsStream(reportPath + "/RepEscuelas.jasper");

		// Opción B: Se carga la plantilla del informe (.jrxml) y se compila "al vuelo":
		// final InputStream reportInputStream =
		// getClass().getResourceAsStream(reportPath + "/RepEscuelas.jrxml");
		// final JasperDesign jasperDesign = JRXmlLoader.load(reportInputStream);
		// JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);

		// Se preapara el datasource del informe con los datos de la escuelas
		final JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(escuelaList);

		// Si el informe tuviera parámetros, se introducen mediante esta estructura:
		Map<String, Object> parameters = new HashMap<>();
		// En el informe se ha definido un parámetro "autor":
		parameters.put("autor", "CEMI");

		try {
			// Genera el PDF a partir de los elementos anteriores
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
					jrBeanCollectionDataSource);

			// Si quisiérmos exportar el PDF a un fichero en la raíz del proyecto:
			//JasperExportManager.exportReportToPdfFile(jasperPrint, "InformeEscuelas.pdf");

			// Convierte el pdf generado en un stream
			JasperExportManager.exportReportToPdfStream(jasperPrint, pdf);

		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getMessage());
		}

		//Devuelve el PDF como un stream
		return pdf.toByteArray();
	}
	
}
