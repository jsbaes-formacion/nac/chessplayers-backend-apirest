package eu.malaga.chessplayers.backend.apirest.servicios;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import java.util.List;
import java.util.Optional;
import java.util.Arrays;
import java.util.Calendar;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import eu.malaga.chessplayers.backend.apirest.dao.IJugadorDao;
import eu.malaga.chessplayers.backend.apirest.entidades.Escuela;
import eu.malaga.chessplayers.backend.apirest.entidades.Jugador;

/**
 * Clase que realiza el test unitario de los métodos de la clase JugadorService
 * 
 * @author 	jsbarriga
 * @version 1.0
 * @since   2019-05-13 
 */
@RunWith(MockitoJUnitRunner.class)
public class JugadorServiceImplTest {

	// Inyectamos el objeto mock que simula la aplicación web cliente
	@InjectMocks
	private JugadorServiceImpl jugadorService; // Clase a probar

	// "Mockeamos" lo que nos va a devolver el DAO que implmenta los métodos del
	@Mock
	private IJugadorDao jugadorDao;

	/**
	 * Prueba del método "recuperaTodos" del servicio que devuelve la lista de jugadores existentes
	 */
	@Test
	public void recuperaTodos_basic() {
		// Se define un objeto Calendar para incializar correctamente la fecha de nacimiento de un jugador
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2018);
		cal.set(Calendar.MONTH, Calendar.DECEMBER);
		cal.set(Calendar.DAY_OF_MONTH, 15);

		// Lista de jugadores que utilizaremos para las pruebas
		List<Jugador> aJugadores = Arrays.asList(
				new Jugador(1L, "nombre 1", "descripcion 1", "imagen1.jpg", cal.getTime(), new Escuela(1L,"Escuela 1"),"2200"),
				new Jugador(2L, "nombre 2", "descripcion 2", "imagen2.jpg", cal.getTime(), new Escuela(2L,"Escuela 2"),""));
		

		// Cuando se llame al método "findAll" del DAO, simularemos 
		// su comportamient devolviendo la lista de jugadores anterior
		when(jugadorDao.findAll()).thenReturn(aJugadores);

		// Llama al servicio a probar
		List<Jugador> jugadores = jugadorService.recuperaTodos();

		// El servicio devuelve el array de jugadoes que le hemos "mockeado"
		assertEquals(aJugadores, jugadores);

		// El servicio tiene que devolver el valor "mockeado"
		assertEquals("2200", jugadores.get(0).getElo());

		// El servicio tiene que devolver "DESCONOCIDO" en el campo elo
		// al estar dicho campo vacío en los datos de prueba "mockeados"
		assertEquals("DESCONOCIDO", jugadores.get(1).getElo());
	}
	
	/**
	 * Prueba del método "recuperaTodos" del servicio que devuelve la lista de jugadores existentes
	 */
	@Test
	public void recuperaTodosPaginado_basic() {
		// Se define un objeto Calendar para incializar correctamente la fecha de nacimiento de un jugador
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2018);
		cal.set(Calendar.MONTH, Calendar.DECEMBER);
		cal.set(Calendar.DAY_OF_MONTH, 15);

		// Lista de jugadores que utilizaremos para las pruebas
		List<Jugador> aJugadores = Arrays.asList(
				new Jugador(1L, "nombre 1", "descripcion 1", "imagen1.jpg", cal.getTime(), new Escuela(1L,"Escuela 1"),"2200"),
				new Jugador(2L, "nombre 2", "descripcion 2", "imagen2.jpg", cal.getTime(),new Escuela(2L,"Escuela 2"),""));
		
		Page<Jugador> paginaJugadores = new PageImpl<Jugador>(aJugadores);

		// Cuando se llame al método "findAll" del DAO, simularemos 
		// su comportamient devolviendo la lista de jugadores anterior
		when(jugadorDao.findAll(PageRequest.of(0,5))).thenReturn(paginaJugadores);

		// Llama al servicio a probar
		Page<Jugador> pjugadores = jugadorService.recuperaTodos(PageRequest.of(0, 5));

		// El servicio devuelve en la página el array de jugadores que le hemos "mockeado"
		assertEquals(aJugadores, pjugadores.getContent());

		// El campo "escuela" del primer jugador devuelto tiene que ser "escuela 1"
		assertEquals("2200", pjugadores.getContent().get(0).getElo());

		// El campo "escuela" del segundo jugador devuelto tiene que ser "DESCONOCIDA"
		assertEquals("DESCONOCIDO", pjugadores.getContent().get(1).getElo());
	}

	/**
	 * Prueba del método "biscarPorId" del servicio que devuelve un jugador por su id
	 */
	@Test
	public void buscarPorId_basic() {
		// Se define un objeto Calendar para incializar correctamente la fecha de naicimiento de un jugador
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2018);
		cal.set(Calendar.MONTH, Calendar.DECEMBER);
		cal.set(Calendar.DAY_OF_MONTH, 15);

		// Jugador que utilizará para las pruebass que utilizaremos para las pruebas
		Jugador pJugador = new Jugador(1L, "nombre 1", "descripcion 1", "imagen1.jpg", cal.getTime(), new Escuela(1L,"Escuela 1"),"2200");

		// Cuando se llame al método "findById" del DAO, simularemos 
		// su comportamient devolviendo el jugador anterior
		when(jugadorDao.findById(1L)).thenReturn(Optional.of(pJugador));

		// El servicio devuelve el jugador que le hemos "mockeado"
		assertEquals(pJugador, jugadorService.buscarPorId(1L));
	}

	/**
	 * Prueba del método "guardar" que da de alta un jugador o actualiza uno existente
	 */
	@Test
	public void guardar_basic() {
		// Se define un objeto Calendar para incializar correctamente la fecha de naicimiento de un jugador
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2018);
		cal.set(Calendar.MONTH, Calendar.DECEMBER);
		cal.set(Calendar.DAY_OF_MONTH, 15);

		// Jugador que utilizará para las pruebass que utilizaremos para las pruebas
		Jugador pJugador = new Jugador(1L, "nombre 1", "descripcion 1", "imagen1.jpg", cal.getTime(),new Escuela(1L,"Escuela 1"),"2200");
		
		// Cuando se llame al método "save" del DAO, simularemos 
		// su comportamient devolviendo el jugador anterior
		when(jugadorDao.save(pJugador)).thenReturn(pJugador);
		
		// El servicio devuelve el jugador que le hemos "mockeado"
		assertEquals(pJugador, jugadorService.guardar(pJugador));
	}
	/**
	 * Prueba del método "guardar" que da de alta un jugador o actualiza uno existente
	 */
	@Test
	public void borrar_basic() {
		
		// No debe hacer nada cuando se llame al método "delete" del DAO
		doNothing().when(jugadorDao).deleteById(1L);
		
		// Se llamada al método a probar del servicio
		jugadorService.borrar(1L);	
		
		// Se verifica que el método del dao es llamado exactamente una vez con el parámetro adecuado
		Mockito.verify(jugadorDao,Mockito.times(1)).deleteById(1L);
	}
}
