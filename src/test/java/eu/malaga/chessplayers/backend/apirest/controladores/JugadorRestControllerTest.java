package eu.malaga.chessplayers.backend.apirest.controladores;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Calendar;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import eu.malaga.chessplayers.backend.apirest.entidades.Escuela;
import eu.malaga.chessplayers.backend.apirest.entidades.Jugador;
import eu.malaga.chessplayers.backend.apirest.servicios.IJugadorService;


/**
 * Clase que realiza el test unitario de los métodos de la clase JugadorRestController
 * 
 * @author 	jsbarriga
 * @version 1.0
 * @since   2019-05-13 
 */
@RunWith(SpringRunner.class)  
@WebMvcTest(JugadorRestController.class)  //Simulamos una aplicacion web que llama a nuestro controlador
public class JugadorRestControllerTest {

	// Inyectamos el objeto mock que simula la aplicación web cliente
	@Autowired
	MockMvc mockMvc;

	// Objeto mock que va asimular la llamada al servcio
	@MockBean
	private IJugadorService jugadorService;

	/**
	 * Método que prueba el método index del controlador que devuelve la lista de jugadores existentes
	 * @throws Exception Excepción que se propaga en caso de producirse
	 */
	@Test
	public void index_basic() throws Exception {
		// Se define un objeto Calendar para incializar correctamente la fecha de naicimiento de un jugador
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2018);
		cal.set(Calendar.MONTH, Calendar.DECEMBER);
		cal.set(Calendar.DAY_OF_MONTH, 15);
		cal.set(Calendar.HOUR_OF_DAY, cal.getActualMinimum(Calendar.HOUR_OF_DAY) + 1);
		cal.set(Calendar.MINUTE, cal.getActualMinimum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getActualMinimum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, cal.getActualMinimum(Calendar.MILLISECOND));

		// Lista de jugadores que utilizaremos para las pruebas
		List<Jugador> aJugadores = Arrays.asList(
				new Jugador(1L, "nombre 1", "descripcion 1", "imagen1.jpg", cal.getTime(), new Escuela(1L,"Escuela 1"),"2200"),
				new Jugador(2L, "nombre 2", "descripcion 2", "imagen2.jpg", cal.getTime(), new Escuela(2L,"Escuela 2"),"2200"));

		// Cuando se llame al método "recuperaTodods" del servicio de jugadores, 
		// simularemos su comportamient devolviendo la lista de jugadores anterior
		when(jugadorService.recuperaTodos()).thenReturn(aJugadores);

		// Simulamos la llamada al verbo GET del controlador con la URL correspondiente
		RequestBuilder request = MockMvcRequestBuilders.get("/api/players").accept(MediaType.APPLICATION_JSON);
		
		// Llamamos al controlador con el objeto simulado definido anteriormente y esperamos 
		// que devuelva un status 200 (OK) y un JSON con la lista de dos jugadores que devuelve el servicio preparado
		mockMvc.perform(request).andExpect(status().isOk()).andExpect(content().json("[{id:1" 
				+ ", nombre: \"nombre 1\"" + ", descripcion: \"descripcion 1\"" + ", img: \"imagen1.jpg\""
				+ ", nacimiento: \"2018-12-15T00:00:00.000+0000\"" + ", escuela: {id:1, descripcion: \"Escuela 1\"}" 
				+ ", elo: \"2200\"}" + ", {id: 2"
				+ ", nombre: \"nombre 2\"" + ", descripcion: \"descripcion 2\"" + ", img: \"imagen2.jpg\""
				+ ", nacimiento: \"2018-12-15T00:00:00.000+0000\"" + ", escuela: {id:2, descripcion: \"Escuela 2\"}"
				+ ", elo: \"2200\"}]")).andReturn();
		

	}

	/**
	 * Método que prueba el método index del controlador que devuelve la lista de jugadores existentes
	 * @throws Exception Excepción que se propaga en caso de producirse
	 */
	@Test
	public void index_paginado_basic() throws Exception {
		// Se define un objeto Calendar para incializar correctamente la fecha de naicimiento de un jugador
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2018);
		cal.set(Calendar.MONTH, Calendar.DECEMBER);
		cal.set(Calendar.DAY_OF_MONTH, 15);
		cal.set(Calendar.HOUR_OF_DAY, cal.getActualMinimum(Calendar.HOUR_OF_DAY) + 1);
		cal.set(Calendar.MINUTE, cal.getActualMinimum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getActualMinimum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, cal.getActualMinimum(Calendar.MILLISECOND));
		
		// Lista de jugadores que utilizaremos para las pruebas
		List<Jugador> aJugadores = Arrays.asList(
				new Jugador(1L, "nombre 1", "descripcion 1", "imagen1.jpg", cal.getTime(), new Escuela(1L,"Escuela 1"),"2200"),
				new Jugador(2L, "nombre 2", "descripcion 2", "imagen2.jpg", cal.getTime(), new Escuela(2L,"Escuela 2"),"2200"));

		Page<Jugador> paginaJugadores = new PageImpl<Jugador>(aJugadores);
		
		// Cuando se llame al método "recuperaTodods" del servicio de jugadores, 
		// simularemos su comportamient devolviendo la lista de jugadores anterior
		when(jugadorService.recuperaTodos(PageRequest.of(0, 5))).thenReturn(paginaJugadores);

		// Simulamos la llamada al verbo GET del controlador con la URL correspondiente
		RequestBuilder request = MockMvcRequestBuilders.get("/api/players/page/0").accept(MediaType.APPLICATION_JSON);
		
		// Llamamos al controlador con el objeto simulado definido anteriormente y esperamos 
		// que devuelva un status 200 (OK) y un JSON con la lista de dos jugadores que devuelve el servicio preparado
		mockMvc.perform(request).andExpect(status().isOk()).andExpect(content().json(
				"{content: [{id:1" 
				+ ", nombre: \"nombre 1\"" 
				+ ", descripcion: \"descripcion 1\"" 
				+ ", img: \"imagen1.jpg\""
				+ ", nacimiento: \"2018-12-15T00:00:00.000+0000\"" 
				+ ", escuela: {id:1, descripcion: \"Escuela 1\"}"
				+ ", elo: \"2200\"}"
				+ ", {id: 2"
				+ ", nombre: \"nombre 2\"" 
				+ ", descripcion: \"descripcion 2\"" 
				+ ", img: \"imagen2.jpg\""
				+ ", nacimiento: \"2018-12-15T00:00:00.000+0000\"" 
				+ ", escuela: {id:2, descripcion: \"Escuela 2\"}"
				+ ", elo: \"2200\"}]"
				+ ", pageable: \"INSTANCE\""
				+ ", totalPages: 1, last: true, totalElements: 2, size: 0"
				+ ", numberOfElements: 2, first: true, sort: {sorted: false, unsorted: true, empty: true}"
				+ ", number: 0, empty: false}")).andReturn();
	}
	
	/**
	 * Método que prueba el método show del controlador que devuelve un jugador a partir de su id
	 * @throws Exception Excepción que se propaga en caso de producirse
	 */
	@Test
	public void show_basic() throws Exception {
		// Se define un objeto Calendar para incializar correctamente la fecha de naicimiento de un jugador
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2018);
		cal.set(Calendar.MONTH, Calendar.DECEMBER);
		cal.set(Calendar.DAY_OF_MONTH, 15);
		cal.set(Calendar.HOUR_OF_DAY, cal.getActualMinimum(Calendar.HOUR_OF_DAY) + 1);
		cal.set(Calendar.MINUTE, cal.getActualMinimum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getActualMinimum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, cal.getActualMinimum(Calendar.MILLISECOND));

		// Jugador que utilizará para las pruebass que utilizaremos para las pruebas
		Jugador pJugador = new Jugador(1L, "nombre 1", "descripcion 1", "imagen1.jpg", cal.getTime(), new Escuela(1L,"Escuela 1"),"2200");

		// Cuando se llame al método "buscarPorId" del servicio de jugadores, 
		// simularemos su comportamient devolviendo el jugador anterior
		when(jugadorService.buscarPorId(1L)).thenReturn(pJugador);

		// Simulamos la llamada al verbo GET del controlador con la URL correspondiente
		RequestBuilder request = MockMvcRequestBuilders.get("/api/players/1").accept(MediaType.APPLICATION_JSON);

		// Llamamos al controlador con el objeto simulado definido anteriormente y esperamos 
		// que devuelva un status 200 (OK) y un JSON con el jugador cuyo id hemos pasado en la URL
		mockMvc.perform(request).andExpect(status().isOk()).andExpect(content().json("{id:1"
				+ ", nombre: \"nombre 1\"" + ", descripcion: \"descripcion 1\"" + ", img: \"imagen1.jpg\""
				+ ", nacimiento: \"2018-12-15T00:00:00.000+0000\"" + ", escuela: {id:1, descripcion: \"Escuela 1\"}"
				+ ", elo: \"2200\"}")).andReturn();
	}

	/**
	 * Método que prueba el método create del controlador que da de alta un jugador
	 * @throws Exception Excepción que se propaga en caso de producirse
	 */
	@Test
	public void create_basic() throws Exception {
		// Se define un objeto Calendar para incializar correctamente la fecha de naicimiento de un jugador
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2018);
		cal.set(Calendar.MONTH, Calendar.DECEMBER);
		cal.set(Calendar.DAY_OF_MONTH, 15);
		cal.set(Calendar.HOUR_OF_DAY, cal.getActualMinimum(Calendar.HOUR_OF_DAY) + 1);
		cal.set(Calendar.MINUTE, cal.getActualMinimum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getActualMinimum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, cal.getActualMinimum(Calendar.MILLISECOND));

		// Jugador que utilizrá para las pruebass que utilizaremos para las pruebas
		Jugador pJugador = new Jugador(1L, "nombre 1", "descripcion 1", "imagen1.jpg", cal.getTime(), new Escuela(1L,"Escuela 1"),"2200");

		// Cuando se llame al método "guardar" del servicio de jugadores, 
		// simularemos su comportamient devolviendo el jugador anterior
		when(jugadorService.guardar(Mockito.any())).thenReturn(pJugador);

		// Simulamos la llamada al verbo POST del controlador con la URL correspondiente
		// y enviando en el cuerpo de la llamada un JSON con los datos del jugador a dar de alta
		RequestBuilder request = MockMvcRequestBuilders.post("/api/players")
				.accept(MediaType.APPLICATION_JSON)
				.content("{\"id\":1" 
						+ ", \"nombre\": \"nombre 1\"" 
						+ ", \"descripcion\": \"descripcion 1\""
						+ ", \"img\": \"imagen1.jpg\"" 
						+ ", \"nacimiento\": \"2018-12-15T00:00:00.000+0000\""
						+ ", \"escuela\": {\"id\":1" + ", \"descripcion\": \"Escuela 1\"}"
						+ ", \"elo\": \"2200\"}")
				.contentType(MediaType.APPLICATION_JSON);

		// Llamamos al controlador con el objeto simulado definido anteriormente y esperamos 
		// que devuelva un status 201 (CREATED) y un JSON con el jugador dado de alta y el
		// mensaje de confirmación indicando que todo ha ido bien.
		mockMvc.perform(request)
					.andExpect(status().isCreated())
					.andExpect(content().json("{\"jugador\":{id:1" 
						+ ", nombre: \"nombre 1\"" 
						+ ", descripcion: \"descripcion 1\""
						+ ", img: \"imagen1.jpg\"" 
						+ ", nacimiento: \"2018-12-15T00:00:00.000+0000\""
						+ ", escuela: {id:1, descripcion: \"Escuela 1\"}" 
						+ ", elo: \"2200\"}, \"mensaje\": \"El jugador ha sido creado con éxito\"}"))
				.andReturn();
	}
	/**
	 * Método que prueba el método update del controlador que actualiza un jugador
	 * @throws Exception Excepción que se propaga en caso de producirse
	 */
	@Test
	public void update_basic() throws Exception {
		// Se define un objeto Calendar para incializar correctamente la fecha de naicimiento de un jugador
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2018);
		cal.set(Calendar.MONTH, Calendar.DECEMBER);
		cal.set(Calendar.DAY_OF_MONTH, 15);
		cal.set(Calendar.HOUR_OF_DAY, cal.getActualMinimum(Calendar.HOUR_OF_DAY) + 1);
		cal.set(Calendar.MINUTE, cal.getActualMinimum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getActualMinimum(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, cal.getActualMinimum(Calendar.MILLISECOND));

		// Jugador que utilizrá para las pruebass que utilizaremos para las pruebas
		Jugador pJugador = new Jugador(1L, "nombre 1", "descripcion 1", "imagen1.jpg", cal.getTime(), new Escuela(1L,"Escuela 1"),"2200");

		// Cuando se llame al método "buscarPorId" del servicio de jugadores, 
		// simularemos su comportamient devolviendo el jugador anterior, Igualmente
		// las llamdas al método "guardar" deolverán también el jugadoar anterior.
		when(jugadorService.buscarPorId(1L)).thenReturn(pJugador);
		when(jugadorService.guardar(Mockito.any())).thenReturn(pJugador);

		// Simulamos la llamada al verbo PUT del controlador con la URL correspondiente
		// y enviando en el cuerpo de la llamada un JSON con los datos del jugador a modificar
		RequestBuilder request = MockMvcRequestBuilders.put("/api/players/1")
				.accept(MediaType.APPLICATION_JSON)
				.content("{\"id\":1" 
						+ ", \"nombre\": \"nombre 1\"" 
						+ ", \"descripcion\": \"descripcion 1\""
						+ ", \"img\": \"imagen1.jpg\"" 
						+ ", \"nacimiento\": \"2018-12-15T00:00:00.000+0000\""
						+ ", \"escuela\": {\"id\":1" + ", \"descripcion\": \"Escuela 1\"}"
						+ ", \"elo\": \"2200\"}")
				.contentType(MediaType.APPLICATION_JSON);

		// Llamamos al controlador con el objeto simulado definido anteriormente y esperamos 
		// que devuelva un status 201 (CREATED) y un JSON con el jugador modificado y el 
		// mensaje de confirmación indicando que todo ha ido bien.
		mockMvc.perform(request)
					.andExpect(status().isCreated())
					.andExpect(content().json("{\"jugador\":{id:1" 
						+ ", nombre: \"nombre 1\"" 
						+ ", descripcion: \"descripcion 1\""
						+ ", img: \"imagen1.jpg\"" 
						+ ", escuela: {id:1, descripcion: \"Escuela 1\"}" 
						+ ", elo: \"2200\"}, \"mensaje\": \"El jugador ha sido actualizado con éxito\"}"))
				.andReturn();
	}
	
	/**
	 * Método que prueba el método delete del controlador que borra un jugador
	 * @throws Exception Excepción que se propaga en caso de producirse
	 */
	@Test
	public void delete_basic() throws Exception {
		// Cuando se llame al método "borrar" del servicio de jugadores, 
		// el servicio no debe hacer nada, ya que la versión real borraría
		// el registro de la Base de Datos.
		doNothing().when(jugadorService).borrar(1L);
		
		// Simulamos la llamada al verbo DELETE del controlador con la URL correspondiente
		// en la que se especifica el id del registro a borrar
		RequestBuilder request = MockMvcRequestBuilders.delete("/api/players/1").accept(MediaType.APPLICATION_JSON);

		// Llamamos al controlador con el objeto simulado definido anteriormente y esperamos 
		// que devuelva un status 200 (OK)
		mockMvc.perform(request).andExpect(status().isOk());
	}
}
